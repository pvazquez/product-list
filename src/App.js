import React, { useState } from 'react';
import AddPoduct from './components/AddProduct';
import List from './components/List';


const Home = () => {

    const [ products, setProducts ] = useState([
      {
        id: 1,
        description: "product1 description",
        img: "product1 path" 
      },
      {
        id: 2,
        description: "product2 description",
        img: "product2 path" 
      },
      {
        id: 3,
        description: "product3 description",
        img: "product3 path" 
      }
    ]);

    const addProduct = () => {
        console.log('Agregando Producto')
    }

    const editItem = (item_data) => {
      console.log(item_data);
      //wip
  }

    const deleteItem = (id) => {
        const items = products.filter(product => product.id !== id);
        setProducts(items);
    }

    return(
        <section>
            <h1>Product List</h1>
            <AddPoduct
                addItem={addProduct}  
            />
            <div className="items-container">
                {
                  products.map( product => 
                      <List
                          {...product}
                          editItem={editItem}
                          deleteItem={deleteItem}
                      />
                  )
                }
            </div>
        </section>
    )
}

export default Home;
