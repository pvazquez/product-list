import React, { useState } from 'react';

const List = props => {

    const [ editForm, setEditForm ] = useState(false)
    const [ form_data, setFormData] = useState({
        item_text: "",
        item_img: ""
    });

    const showForm = () => {
        if(editForm) {
            return (
                <form  onSubmit={handleSubmit}>
                    <input
                        onChange={storeFormData}
                        type="text" 
                        name="item_text"
                        defaultValue={props.description}
                    />
                    <input
                        onChange={storeFormData}
                        type="text" 
                        name="item_img"
                    />

                    <input type="submit"/>
                </form>
            )
        }
    }

    const handleEdit = () => {
        setEditForm(true);
    }

    const storeFormData = (event) => {
        setFormData({
            ...form_data,
            [event.target.name]: event.target.value
        });
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        setEditForm(false);
        props.editItem(form_data);
    }

    const handleDelete = (id) => {
        props.deleteItem(id)
    }

    return(
        <div key={props.id} className="item-box">
            {showForm()}
            <div className="handle-product-div">
                <button 
                    className="button"
                    onClick={ () => handleDelete(props.id)}
                >
                    D
                </button>
                <button 
                    className="button"
                    onClick={handleEdit}
                >
                    E
                </button>
            </div>
            <div>{props.img}</div>
            <div>{props.description}</div>
        </div>
    )
}

export default List